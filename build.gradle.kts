/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

plugins {
    id("church.18n.public.java-project")
    alias(libs.plugins.info.solidsoft.pitest)
}

description = """The library defines a response structure of an application with focus on error 
state of an application."""

//Plugin is applied only after description is set, otherwise it's missing.
apply(plugin = "church.18n.public.java-project-publishable")

sourceSets {
    named("test") {
        java.srcDir("src/test/java")
        resources.srcDirs("src/main/resources", "src/test/resources")
    }
}

dependencies {
    api(project(":resources.bundles"))
    api(project(":processing.exception"))
    api(platform(project(":spring.webmvc.platform")))
    api(libs.slf4j.api)
    implementation(libs.jetbrains.annotations)

    testImplementation(libs.junit.jupiter)
    testImplementation(libs.archunit)
    testImplementation(libs.bundles.mockito)
    testImplementation(libs.equalsverifier)
    testImplementation(libs.openpojo)
}

tasks.processTestResources {
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
}

pitest {
    // Adds dependency to org.pitest:pitest-junit5-plugin and sets "testPlugin" to "junit5"
    junit5PluginVersion.set(libs.versions.pitest.junit5.plugin.get())
    targetClasses.set(listOf("church.i18n.response.utils.*"))
    outputFormats.set(listOf("XML", "HTML"))
    threads.set(4)
    timestampedReports.set(false)
    // mutators = ["ALL"]
    // pitestVersion = "1.4.1" // Not needed when a default PIT version should be used
    // testSourceSets = [sourceSets.test, sourceSets.integrationTest]
    // mainSourceSets = [sourceSets.main, sourceSets.additionalMain]
    // jvmArgs = ["-Xmx1024m"]
    // useClasspathFile = true     // Useful with bigger projects on Windows
    // fileExtensionsToFilter.addAll("xml", "orbit")
}

// Set the order of tasks
tasks.named("build") {
    dependsOn(tasks.named("pitest"))
}
