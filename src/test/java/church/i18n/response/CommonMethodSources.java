/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response;

import static church.i18n.processing.message.MessageStatus.ERROR;
import static church.i18n.processing.message.MessageStatus.WARNING;

import church.i18n.processing.exception.ProcessingException;
import church.i18n.processing.message.ContextInfo;
import church.i18n.processing.message.ContextValue;
import church.i18n.processing.message.I18nMessage;
import church.i18n.processing.message.MessageStatus;
import church.i18n.processing.message.ProcessingMessage;
import church.i18n.processing.status.ClientError;
import church.i18n.response.domain.ContextInfoDto;
import church.i18n.response.domain.ResponseDto;
import church.i18n.response.domain.ResponseMessageDto;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Stream;
import org.junit.jupiter.params.provider.Arguments;

public final class CommonMethodSources {

  private static final String NAME = "name";
  private static final String TYPE = "type";
  private static final String VALUE = "value";
  private static final String FORMAT = "format";
  private static final String FORMAT_TYPE = "formatType";
  private static final ContextInfo CONTEXT_INFO = ContextInfo
      .of(NAME, new ContextValue(VALUE, TYPE));
  private static final ContextValue CONTEXT_VALUE = new ContextValue(FORMAT, FORMAT_TYPE);
  private static final I18nMessage contextMessage = new I18nMessage("{0} {1} {0,number,currency}",
      123345, "as currency:");
  //copy builder
  private static final ContextInfo copyBuilderContextInfo = ContextInfo
      .builder(Objects.requireNonNull(CONTEXT_INFO))
      .withHelp(CONTEXT_VALUE)
      .withMessage(contextMessage)
      .build();

  private CommonMethodSources() {
    throw new IllegalStateException("Utility class");
  }

  public static Stream<Arguments> exceptionSources() throws URISyntaxException {
    ArrayList<Object> nullLocale = new ArrayList<>(1);
    nullLocale.add(null);
    //@formatter:off
    assert copyBuilderContextInfo != null;return Stream.of(/*I18nResponseException constructors*/
        Arguments.of(new ProcessingException("msg-A1"), List.of(), List.of(Locale.FRANCE),
            new ResponseDto<>("requestId", "data",
                new ResponseMessageDto("msg-A1", "appartement", MessageStatus.ERROR.name(), null,
                    List.of()), List.of())),
        Arguments.of(new ProcessingException("msg-A1", new Throwable("Sample exception"))
                .withMessageType(WARNING), List.of(), List.of(Locale.FRANCE),
            new ResponseDto<>("requestId", "data",
                new ResponseMessageDto("msg-A1", "appartement", WARNING.name(),
                    null,
                    List.of()), List.of())),
        Arguments.of(new ProcessingException("msg-A2", 1234567.987), List.of(),
            List.of(Locale.TRADITIONAL_CHINESE), new ResponseDto<>("requestId", "data",
                new ResponseMessageDto("msg-A2", "套間: $1,234,567.99", MessageStatus.ERROR.name(),
                    null,
                    List.of()), List.of())),
        Arguments
            .of(new ProcessingException("msg-A2", 1234567.987, new Throwable("Sample exception")),
                List.of(), List.of(Locale.TRADITIONAL_CHINESE),
                new ResponseDto<>("requestId", "data",
                    new ResponseMessageDto("msg-A2", "套間: $1,234,567.99",
                        MessageStatus.ERROR.name(),
                        null, List.of()), List.of())),
        /*I18nResponseException builders*/
        /*#5*/
        Arguments.of(
            new ProcessingException("msg-A1", new Throwable()).withMessageType(MessageStatus.INFO)
                .withHelpUri("g.com").withStatus(ClientError.FORBIDDEN).addContextInfo(List.of(
                Objects.requireNonNull(ContextInfo.of(NAME).withContext(new ContextValue(VALUE, TYPE))
                    .withHelp(FORMAT, FORMAT_TYPE)
                    .withMessage("Address ''{0}'' already exists in the system",
                        "His Holiness Pope Francis / Vatican City State, 00120").build()))), List.of(
                ProcessingMessage.withMessage("msg-A2", 123)
                    .withMessageType(WARNING)
                    .withHelpUri(new URI("i18n.church")).addContextInfo(List.of(
                    Objects.requireNonNull(ContextInfo.of(NAME).withContext(new ContextValue(VALUE, TYPE))
                        .withHelp(new ContextValue(FORMAT, FORMAT_TYPE)).withMessage(
                        new I18nMessage("Address ''{0}'' already exists in the system", ""))
                        .build()),
                    Objects.requireNonNull(ContextInfo.of(NAME).withMessage(new I18nMessage("msg")).build()),
                    new ContextInfo(NAME, null, null, null, null),
                    Objects.requireNonNull(ContextInfo.builder(copyBuilderContextInfo).build())))
                    .addContextInfo(copyBuilderContextInfo)
                    .addContextInfo(List.of(copyBuilderContextInfo))
                    .build()),
            List.of(Locale.FRANCE), new ResponseDto<>("requestId", "data",
                new ResponseMessageDto("msg-A1", "appartement", MessageStatus.INFO.name(),
                    "g.com",
                    List.of(
                        new ContextInfoDto(NAME, VALUE, TYPE, FORMAT, FORMAT_TYPE,
                            "Address 'His Holiness Pope Francis / Vatican City State, 00120' already exists in the system"))),
                List.of(
                    new ResponseMessageDto("msg-A2", "appartement: 123,00 €",
                        WARNING.name(),
                        "i18n.church", List.of(
                        new ContextInfoDto(NAME, VALUE, TYPE, FORMAT, FORMAT_TYPE,
                            "Address '' already exists in the system"),
                        new ContextInfoDto(NAME, null, null, null, null, "apéritif"),
                        new ContextInfoDto(NAME, null, null, null, null, null),
                        new ContextInfoDto(NAME, VALUE, TYPE, FORMAT, FORMAT_TYPE,
                            "123,345 as currency: ¤ 123,345.00"),
                        new ContextInfoDto(NAME, VALUE, TYPE, FORMAT, FORMAT_TYPE,
                            "123,345 as currency: ¤ 123,345.00"),
                        new ContextInfoDto(NAME, VALUE, TYPE, FORMAT, FORMAT_TYPE,
                            "123,345 as currency: ¤ 123,345.00")
                    ))))),
        /*#6 */
        Arguments.of(new ProcessingException("msg-A2", 87123.091237)
                .addContextInfo((ContextInfo[]) null), List.of(), List.of(Locale.FRANCE),
            new ResponseDto<>("requestId", "data",
                new ResponseMessageDto("msg-A2", "appartement: 87 123,09 €",
                    ERROR.name(),
                    null, List.of()), List.of())),
        Arguments.of(new ProcessingException("msg-A2", 1,
                new UnsupportedOperationException("Sample exception")), List.of(),
            List.of(Locale.FRANCE), new ResponseDto<>("requestId", "data",
                new ResponseMessageDto("msg-A2", "appartement: 1,00 €",
                    ERROR.name(), null,
                    List.of()), List.of())),
        Arguments.of(new ProcessingException("msg-A2", 123.123,
                new UnsupportedOperationException("Source cause")), List.of(),
            List.of(Locale.ENGLISH),
            new ResponseDto<>("requestId",
                "data",/*¤ is a currency symbol for locale English (no country): http://www.localeplanet.com/icu/en/index.html*/
                new ResponseMessageDto("msg-A2", "flat-english: ¤123.12",
                    ERROR.name(),
                    null,
                    List.of()), List.of())),
        Arguments.of(new ProcessingException("msg-A1",
                new UnsupportedOperationException("Source cause")), List.of(), List.of(Locale.ENGLISH),
            new ResponseDto<>("requestId", "data",
                new ResponseMessageDto("msg-A1", "flat-english", ERROR.name(), null,
                    List.of()), List.of())),
        /*#10*/
        Arguments.of(new ProcessingException("msg-A1",
                new UnsupportedOperationException("Source cause")), List.of(), nullLocale,
            new ResponseDto<>("requestId", null,
                new ResponseMessageDto("msg-A1", "apartment", ERROR.name(), null,
                    List.of()), List.of())),
        Arguments.of(new ProcessingException("msg-A1", (Object[]) null), List.of(),
            List.of(Locale.ENGLISH), new ResponseDto<>("requestId", null,
                new ResponseMessageDto("msg-A1", "flat-english", ERROR.name(), null,
                    List.of()), List.of())),
        Arguments
            .of(new ProcessingException(ProcessingMessage.withMessage("msg-A1").build()), List.of(),
                List.of(Locale.ENGLISH), new ResponseDto<>("requestId", null,
                    new ResponseMessageDto("msg-A1", "flat-english", ERROR.name(),
                        null,
                        List.of()), List.of())),
        Arguments.of(new ProcessingException(ProcessingMessage.withMessage("msg-A1").build(),
                new Throwable("The cause")), List.of(), List.of(Locale.ENGLISH),
            new ResponseDto<>("requestId",
                new ResponseMessageDto("msg-A1", "flat-english", ERROR.name(), null,
                    List.of()), List.of())),
        /*#14*/
        Arguments.of(
            new ProcessingException("msg-A1").withHelpUri(new URI("i18n.church"))
                .addContextInfo(copyBuilderContextInfo)
                .addContextInfo(ContextInfo.of(NAME).build()),
            List.of(ProcessingMessage.withMessage("msg").build(),
                ProcessingMessage.withMessage("msg-A1").build(),
                ProcessingMessage.withMessage("msg-A2", 11).build()), List.of(Locale.ENGLISH),
            new ResponseDto<>("requestId",
                new ResponseMessageDto("msg-A1", "flat-english", ERROR.name(),
                    "i18n.church",
                    List.of(new ContextInfoDto(NAME, VALUE, TYPE, FORMAT, FORMAT_TYPE,
                            "123,345 as currency: ¤ 123,345.00"),
                        new ContextInfoDto(NAME, null, null, null, null, null))), List.of(
                new ResponseMessageDto("msg", "appetizer-english", ERROR.name(),
                    null,
                    List.of()),
                new ResponseMessageDto("msg-A1", "flat-english", ERROR.name(), null,
                    List.of()),/*¤ is a currency symbol for locale English (no country): http://www.localeplanet.com/icu/en/index.html*/
                new ResponseMessageDto("msg-A2", "flat-english: ¤11.00",
                    ERROR.name(),
                    null,
                    List.of())))));
  }

  //@formatter:on
}
