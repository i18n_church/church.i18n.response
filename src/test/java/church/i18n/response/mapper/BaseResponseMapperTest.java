/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.mapper;

import static church.i18n.processing.security.policy.SecurityLevel.PUBLIC;
import static church.i18n.processing.security.policy.SecurityLevel.SYSTEM_EXTERNAL;
import static church.i18n.processing.security.policy.SecurityLevel.SYSTEM_INTERNAL;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;

import church.i18n.processing.config.DefaultProcessingExceptionConfig;
import church.i18n.processing.config.ProcessingExceptionConfig;
import church.i18n.processing.exception.ProcessingException;
import church.i18n.processing.logger.MessageTypeLogMapper;
import church.i18n.processing.message.MessageStatus;
import church.i18n.processing.message.ProcessingMessage;
import church.i18n.processing.security.sanitizer.DefaultSecurityInfoSanitizer;
import church.i18n.processing.security.sanitizer.SecurityInfoSanitizer;
import church.i18n.processing.storage.ProcessingIdProvider;
import church.i18n.processing.storage.ThreadLocalStorage;
import church.i18n.resources.bundles.PolyglotMultiResourceBundle;
import church.i18n.response.domain.Response;
import church.i18n.response.domain.ResponseDto;
import church.i18n.response.domain.ResponseMessageDto;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

class BaseResponseMapperTest {

  private static final ProcessingIdProvider rip = () -> "requestId";
  private static final ThreadLocalStorage threadLocalStorage = new ThreadLocalStorage(rip);
  private BaseResponseMapper mapper;

  @BeforeEach
  void setUp() {
    ProcessingExceptionConfig config = DefaultProcessingExceptionConfig.builder()
        .withDefaultMessageType(MessageStatus.ERROR)
        .withDefaultSecurityPolicy(SYSTEM_EXTERNAL)
        .withExposeSecurityPolicies(Set.of(SYSTEM_INTERNAL, PUBLIC, SYSTEM_EXTERNAL))
        .build();
    SecurityInfoSanitizer logSanitizer = new DefaultSecurityInfoSanitizer(
        ProcessingExceptionConfig::defaultSecurityPolicy,
        ProcessingExceptionConfig::logSecurityPolicies
    );
    this.mapper = BaseResponseMapper.builder()
        .withLogMapper(new MessageTypeLogMapper(logSanitizer, config))
        .withConfig(config)
        .withProcessingIdProvider(rip)
        .withExposeSanitizer(new DefaultSecurityInfoSanitizer(
            ProcessingExceptionConfig::defaultSecurityPolicy,
            ProcessingExceptionConfig::exposeSecurityPolicies
        ))
        .withMessageStorage(threadLocalStorage)
        .withBundle(new PolyglotMultiResourceBundle("i18n.A", "i18n.errors-test")
            .setDefaultLocale(Locale.US))
        .build();
  }

  @Test
  void loggerIsSet() {
    assertNotNull(this.mapper.getLogMapper());
  }

  @ParameterizedTest
  @MethodSource("church.i18n.response.CommonMethodSources#exceptionSources")
  void mapException(final ProcessingException ex, final List<ProcessingMessage> appendMessages,
      final List<Locale> locales, final Response<Object> response) {
    threadLocalStorage.clearStorage(rip.getProcessingId());
    threadLocalStorage.addMessages(rip.getProcessingId(), appendMessages);
    Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.CANADA);
      Response<?> mappedData = this.mapper.map(response.getData(), ex, locales);
      assertEquals(response, mappedData);
    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @ParameterizedTest
  @MethodSource("church.i18n.response.CommonMethodSources#exceptionSources")
  void mapExceptionLocalesEnumeration(final ProcessingException ex,
      final List<ProcessingMessage> appendMessages, final List<Locale> locales,
      final Response<Object> response) {
    threadLocalStorage.clearStorage(rip.getProcessingId());
    threadLocalStorage.addMessages(rip.getProcessingId(), appendMessages);
    Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.of("en", "US"));
      Response<?> mappedData = this.mapper
          .map(response.getData(), ex, Collections.enumeration(locales));
      assertEquals(response, mappedData);
    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @ParameterizedTest
  @MethodSource("church.i18n.response.CommonMethodSources#exceptionSources")
  void mapExceptionWithoutData(final ProcessingException ex,
      final List<ProcessingMessage> appendMessages, final List<Locale> locales,
      final Response<Object> response) {
    threadLocalStorage.clearStorage(rip.getProcessingId());
    threadLocalStorage.addMessages(rip.getProcessingId(), appendMessages);
    Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.CANADA);
      Response<?> mappedData = this.mapper.map(ex, locales);
      Response<Object> responseNullData = new ResponseDto<>("requestId",
          response.getError(), response.getMessages());
      assertEquals(responseNullData, mappedData);
    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @ParameterizedTest
  @MethodSource("church.i18n.response.CommonMethodSources#exceptionSources")
  void mapExceptionWithoutDataLocalesEnumeration(final ProcessingException ex,
      final List<ProcessingMessage> appendMessages, final List<Locale> locales,
      final Response<Object> response) {
    threadLocalStorage.clearStorage(rip.getProcessingId());
    threadLocalStorage.addMessages(rip.getProcessingId(), appendMessages);
    Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.CANADA);
      Response<?> mappedData = this.mapper.map(ex, Collections.enumeration(locales));
      Response<Object> responseNullData = new ResponseDto<>("requestId",
          response.getError(), response.getMessages());
      assertEquals(responseNullData, mappedData);
    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @ParameterizedTest
  @MethodSource("church.i18n.response.CommonMethodSources#exceptionSources")
  void mapMessages(final ProcessingException ex, final List<ProcessingMessage> appendMessages,
      final List<Locale> locales, final Response<Object> response) {
    threadLocalStorage.clearStorage(rip.getProcessingId());
    threadLocalStorage.addMessages(rip.getProcessingId(), appendMessages);
    Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.CANADA);
      Response<Object> mappedData = this.mapper
          .map(response.getData(), ex.getProcessingMessage(), locales);
      assertEquals(response, mappedData);
    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @ParameterizedTest
  @MethodSource("church.i18n.response.CommonMethodSources#exceptionSources")
  void mapMessagesLocalesEnumeration(final ProcessingException ex,
      final List<ProcessingMessage> appendMessages, final List<Locale> locales,
      final Response<Object> response) {
    threadLocalStorage.clearStorage(rip.getProcessingId());
    threadLocalStorage.addMessages(rip.getProcessingId(), appendMessages);
    Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.CANADA);
      Response<?> mappedData = this.mapper
          .map(response.getData(), ex.getProcessingMessage(),
              Collections.enumeration(locales));
      assertEquals(response, mappedData);
    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @ParameterizedTest
  @MethodSource("church.i18n.response.CommonMethodSources#exceptionSources")
  void mapMessagesWithoutData(final ProcessingException ex,
      final List<ProcessingMessage> appendMessages, final List<Locale> locales,
      final Response<Object> response) {
    threadLocalStorage.clearStorage(rip.getProcessingId());
    threadLocalStorage.addMessages(rip.getProcessingId(), appendMessages);
    Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.CANADA);
      Response<?> mappedData = this.mapper
          .map(ex.getProcessingMessage(), locales);
      Response<Object> responseNullData = new ResponseDto<>("requestId",
          response.getError(), response.getMessages());
      assertEquals(responseNullData, mappedData);
    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @ParameterizedTest
  @MethodSource("church.i18n.response.CommonMethodSources#exceptionSources")
  void mapMessagesWithoutDataLocalesEnumeration(final ProcessingException ex,
      final List<ProcessingMessage> appendMessages, final List<Locale> locales,
      final Response<Object> response) {
    threadLocalStorage.clearStorage(rip.getProcessingId());
    threadLocalStorage.addMessages(rip.getProcessingId(), appendMessages);
    Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.CANADA);
      Response<?> mappedData = this.mapper
          .map(ex.getProcessingMessage(), Collections.enumeration(locales));
      Response<Object> responseNullData = new ResponseDto<>("requestId",
          response.getError(), response.getMessages());
      assertEquals(responseNullData, mappedData);
    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @Test
  void testMessagesAreAppendedFromResponseMessageStorage() {
    threadLocalStorage.addMessages(rip.getProcessingId(), new ProcessingMessage("msg"));
    Response<?> data = this.mapper
        .map("data", new ProcessingException("msg-A1"), List.of(Locale.ENGLISH));
    ResponseDto<String> expectedData = new ResponseDto<>("requestId", "data",
        new ResponseMessageDto("msg-A1", "flat-english", MessageStatus.ERROR.name(), null,
            List.of()),
        List.of(
            new ResponseMessageDto("msg", "appetizer-english", MessageStatus.ERROR.name(), null,
                List.of())
        ));
    assertEquals(expectedData, data);
  }

  @Test
  void testNullErrorMessage() {
    Response<?> data = this.mapper.map("data", (ProcessingException) null, List.of());
    ResponseDto<String> expectedData = new ResponseDto<>("requestId", "data", List.of());
    assertEquals(expectedData, data);
  }

  @Test
  void toResponse_nullMessage_nullData() {

    ProcessingExceptionConfig config = DefaultProcessingExceptionConfig.builder()
        .withDefaultMessageType(MessageStatus.ERROR)
        .withDefaultSecurityPolicy(SYSTEM_EXTERNAL)
        .withExposeSecurityPolicies(Set.of(SYSTEM_INTERNAL, PUBLIC, SYSTEM_EXTERNAL))
        .build();
    SecurityInfoSanitizer DENY_ALL = (infoSecurityPolicy, _cfg) -> false;
    BaseResponseMapper responseMapper = BaseResponseMapper.builder()
        .withLogMapper(new MessageTypeLogMapper(DENY_ALL, config))
        .withConfig(config)
        .withProcessingIdProvider(rip)
        .withExposeSanitizer(DENY_ALL)
        .withMessageStorage(threadLocalStorage)
        .withBundle(new PolyglotMultiResourceBundle("i18n.A", "i18n.errors-test")
            .setDefaultLocale(Locale.US))
        .build();

    Response<Object> response = responseMapper.toResponse("requestId", null, null, List.of());
    //@Nullable final Object data, @Nullable final ProcessingMessage error,
    //@Nullable final List<Locale> locales, @Nullable final ProcessingMessage...messages)

    assertEquals("requestId", response.getRequestId());
    assertNull(response.getData());
    assertNull(response.getError());
    assertEquals(response.getMessages(), List.of());
  }

  @Test
  void getters() {
    ProcessingExceptionConfig config = DefaultProcessingExceptionConfig.builder()
        .withDefaultMessageType(MessageStatus.ERROR)
        .withDefaultSecurityPolicy(SYSTEM_EXTERNAL)
        .withExposeSecurityPolicies(Set.of(SYSTEM_INTERNAL, PUBLIC, SYSTEM_EXTERNAL))
        .build();
    SecurityInfoSanitizer DENY_ALL = (infoSecurityPolicy, _cfg) -> false;
    BaseResponseMapper responseMapper = BaseResponseMapper.builder()
        .withLogMapper(new MessageTypeLogMapper(DENY_ALL, config))
        .withConfig(config)
        .withProcessingIdProvider(rip)
        .withExposeSanitizer(DENY_ALL)
        .withMessageStorage(threadLocalStorage)
        .withBundle(new PolyglotMultiResourceBundle("i18n.A", "i18n.errors-test")
            .setDefaultLocale(Locale.US))
        .build();

    assertSame(config, responseMapper.getConfig());
    assertSame(DENY_ALL, responseMapper.getExposeSanitizer());
  }
}
