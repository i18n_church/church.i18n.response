/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertThrows;

import church.i18n.processing.message.I18nMessage;
import church.i18n.resources.bundles.PolyglotMultiResourceBundle;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Locale;
import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

@Execution(ExecutionMode.SAME_THREAD)
class MessageUtilsTest {

  private final PolyglotMultiResourceBundle bundle_US =
      new PolyglotMultiResourceBundle("i18n.A")
          .setDefaultLocale(Locale.US);
  private PolyglotMultiResourceBundle bundle;

  @BeforeEach
  void setUp() {
    Locale defaultLocale = Locale.getDefault();
    try {
      Locale.setDefault(Locale.US);
      bundle = new PolyglotMultiResourceBundle("i18n.A");
    } finally {
      Locale.setDefault(defaultLocale);
    }
  }

  @Test
  void constructorThrowsException() throws NoSuchMethodException {
    Constructor<MessageUtils> c = MessageUtils.class.getDeclaredConstructor();
    c.setAccessible(true);
    final InvocationTargetException exception = assertThrows(
        InvocationTargetException.class, c::newInstance);
    assertInstanceOf(IllegalStateException.class, exception.getCause());
  }

  @ParameterizedTest
  @MethodSource("evaluateMessageSources")
  void evaluateMessage(final String expectedMessage, final I18nMessage message,
      final Locale[] locales) {
    assertEquals(expectedMessage,
        MessageUtils.evaluateMessage(bundle, message, Arrays.asList(locales)));
  }

  @ParameterizedTest
  @MethodSource("evaluateMessageSources")
  void evaluateMessage_manualDefaultLocale(final String expectedMessage,
      final I18nMessage message, final Locale[] locales) {
    assertEquals(expectedMessage,
        MessageUtils.evaluateMessage(bundle_US, message, Arrays.asList(locales)));
  }

  private static Stream<Arguments> evaluateMessageSources() {
    return Stream.of(
        Arguments.of("开胃菜", new I18nMessage("msg"), new Locale[]{Locale.CHINA}),
        Arguments.of("appartement: 1 234,00 €", new I18nMessage("msg-A2", 1234),
            new Locale[]{Locale.CANADA, Locale.FRANCE})
    );
  }
}
