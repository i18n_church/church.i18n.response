/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.domain;

import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Contextual information associated with response message.
 */
public class ContextInfoDto implements ResponseContextInfo, Serializable {

  @Serial
  private static final long serialVersionUID = -6398338285535454399L;
  private final @NotNull String name;
  private final @Nullable Object value;
  private final @Nullable String valueType;
  private final @Nullable Object help;
  private final @Nullable String helpType;
  private final @Nullable String message;

  /**
   * Holds contextual information associated with response message.
   *
   * @param name      Name of the parameter, property or validated field. E.g., email, name.
   * @param value     Value type. For example: BOOLEAN, STRING, OBJECT…
   * @param valueType The original value that was provided by the user, computed value by the system
   *                  or other value associated with the message.
   * @param help      Specifies the correct or expected format of the data. When it is not met, you
   *                  may instruct consumer of the message by specifying the format of the input
   *                  parameter your application expects. E.g., for simple e-mail format it may hold
   *                  regular expression: {@code ^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,64}$}.
   * @param helpType  Format argument type. For example when the format of data type is an object
   *                  you can closely specify what type of object it is — e.g., JSON, XML. Or
   *                  similarly, when you specify the parameter of a string type, you may specify
   *                  that string is an array or enum format.
   * @param message   Message providing more detailed human-readable information about the
   *                  parameter.
   */
  public ContextInfoDto(final @NotNull String name, final @Nullable Object value,
      final @Nullable String valueType, final @Nullable Object help,
      final @Nullable String helpType, final @Nullable String message) {
    this.name = name;
    this.value = value;
    this.valueType = valueType;
    this.help = help;
    this.helpType = helpType;
    this.message = message;
  }

  @Override
  public @Nullable Object getHelp() {
    return help;
  }

  @Override
  public @Nullable String getHelpType() {
    return helpType;
  }

  @Override
  public @Nullable String getMessage() {
    return message;
  }

  @Override
  public @NotNull String getName() {
    return name;
  }

  @Override
  public @Nullable Object getValue() {
    return value;
  }

  @Override
  public @Nullable String getValueType() {
    return valueType;
  }

  @Override
  public final int hashCode() {
    return Objects.hash(name, value, valueType, help, helpType,
        message);
  }

  @Override
  public final boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (!(obj instanceof final ContextInfoDto that)) {
      return false;
    }
    return Objects.equals(name, that.name)
        && Objects.equals(value, that.value)
        && Objects.equals(valueType, that.valueType)
        && Objects.equals(help, that.help)
        && Objects.equals(helpType, that.helpType)
        && Objects.equals(message, that.message);
  }

  @Override
  public String toString() {
    return "ContextInfoDto{" +
        "name='" + name + '\'' +
        ", value=" + value +
        ", valueType='" + valueType + '\'' +
        ", help=" + help +
        ", helpType='" + helpType + '\'' +
        ", message='" + message + '\'' +
        '}';
  }
}
