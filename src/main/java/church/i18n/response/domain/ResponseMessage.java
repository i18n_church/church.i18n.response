/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.domain;

import java.io.Serializable;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


/**
 * Typical structure of response message in the case the application needs to communicate a message
 * to a message consumer. This message could be a primary error response or additional message
 * appended to response. The structure of response message is following:
 * <ol>
 * <li>
 *   <b>code</b>: Code for automatic processing of message returned by the application.
 * </li>
 * <li>
 *   <b>message</b>: Human readable and understandable message. The message should contain enough
 *   information to understand the problem. However, it should not include any sensitive information
 *   that can be used by attacker to compromise system. This includes such as passwords, internal
 *   application structure, libraries, versions… <b>Never trust third-party messages</b>, you do not
 *   have a control over them. Third-party message today may not be the same as of tomorrow.
 *   <b>Please avoid them, this is your responsibility.</b>
 * </li>
 * <li>
 *   <b>severity</b>: Is a severity of the returned message.
 * </li>
 * <li>
 *   <b>helpUri</b>: Is a URI pointing to KB or documentation where user can find more about this
 *   error or message in general.
 * </li>
 * <li>
 *   <b>context</b>: Contains list of variables, usually that are included within the message. You
 *   may include also variables useful for automatic processing. Please avoid a situation when
 *   the consumer of the message needs to parse it, to get information about the state.
 * </li>
 * </ol>
 */
public interface ResponseMessage extends Serializable {

  /**
   * Code for automatic processing of message returned by the application.
   *
   * @return Computer-processable identification of a message.
   */
  @NotNull
  String getCode();

  /**
   * Contains list of variables, usually that are included within the message. You may include also
   * variables useful for automatic processing. Please avoid a situation when the consumer of the
   * message needs to parse it, to get information about the state.
   *
   * @return List of contextual information associated with the message.
   */
  @Nullable
  List<ResponseContextInfo> getContext();

  /**
   * Is a URI pointing to KB or documentation where user can find more about this error or message
   * in general.
   *
   * @return A link where to find more information about the state.
   */
  @Nullable
  String getHelpUri();

  /**
   * Human readable and understandable message. The message should contain enough information to
   * understand the problem. However, it should not include any sensitive information that can be
   * used by attacker to compromise system. This includes such as passwords, internal application
   * structure, libraries, versions… <b>Never trust third-party messages</b>, you do not have a
   * control over them. Third-party message today may not be the same as of tomorrow.
   * <b>Please avoid them, this is your responsibility.</b>
   *
   * @return Human readable and understandable message describing the problem.
   */
  @Nullable
  String getMessage();

  /**
   * MessageSeverity of the returned message.
   *
   * @return MessageSeverity associated with the message.
   */
  @Nullable
  String getType();
}
