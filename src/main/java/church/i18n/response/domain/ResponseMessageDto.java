/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.domain;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Default Rest Error Response in the case the application encounters exception or there is other
 * type of the message that should be returned to the user together with data. This response is
 * holding base information and no more for proper error response:
 * <ol>
 * <li>
 *   <b>code</b>: Code for automatic processing of errors returned by the application.
 * </li>
 * <li>
 *   <b>message</b>: Human readable and understandable message. The message should contain enough
 *   information to understand what was the problem. However it should not include any sensitive
 *   information that can be used by attacker to abuse them. This includes such as passwords,
 *   internal application structure, libraries, versions...
 *   <b>Please avoid them, this is your responsibility.</b>
 * </li>
 * <li>
 *   <b>severity</b>: Is a severity of the returned message.
 * </li>
 * <li>
 *   <b>helpUri</b>: Is an URI into KB or documentation where user can find more about this error.
 * </li>
 * <li>
 *   <b>context</b>: Contains list of variables, usually that are included in the message or are in
 *   other way important for automatic error handling so the consumer of the message is not required
 *   to parse message and have enough information to process the exception. For the structure
 *   {@link ResponseDto}
 * </li>
 * </ol>
 */
public class ResponseMessageDto implements ResponseMessage, Serializable {

  @Serial
  private static final long serialVersionUID = 5756648629963090479L;
  private final @NotNull String code;
  private final @Nullable String message;
  private final @Nullable String type;
  private final @Nullable String helpUri;
  private final @Nullable List<ResponseContextInfo> context;

  /**
   * Base constructor of response message object.
   *
   * @param code    Code for automatic processing of message returned by the application.
   * @param message Human readable and understandable message. The message should contain enough
   *                information to understand the problem. However, it should not include any
   *                sensitive information that can be used by attacker to compromise system. This
   *                includes such as passwords, internal application structure, libraries,
   *                versions…
   *                <b>Never trust third-party messages</b>, you do not have a control over them.
   *                Third-party message today may not be the same as of tomorrow.
   *                <b>Please avoid them, this is your responsibility.</b>
   * @param type    Is a severity of the returned message.
   * @param helpUri Is a URI pointing to KB or documentation where user can find more about this
   *                error or message in general.
   * @param context Contains list of variables, usually that are included within the message. You
   *                may include also variables useful for automatic processing. Please avoid a
   *                situation when the consumer of the message needs to parse it, to get information
   *                about the state.
   */
  public ResponseMessageDto(final @NotNull String code, final @Nullable String message,
      final @Nullable String type, final @Nullable String helpUri,
      final @Nullable List<ResponseContextInfo> context) {
    this.code = code;
    this.message = message;
    this.type = type;
    this.helpUri = helpUri;
    this.context = context;
  }

  @Override
  public @NotNull String getCode() {
    return this.code;
  }

  @Override
  public @Nullable List<ResponseContextInfo> getContext() {
    return this.context;
  }

  @Override
  public @Nullable String getHelpUri() {
    return this.helpUri;
  }

  @Override
  public @Nullable String getMessage() {
    return this.message;
  }

  @Override
  public @Nullable String getType() {
    return this.type;
  }

  @Override
  public final int hashCode() {
    return Objects.hash(this.code, this.message, this.type, this.helpUri, this.context);
  }

  @Override
  public final boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (!(obj instanceof final ResponseMessageDto that)) {
      return false;
    }
    return Objects.equals(this.code, that.code)
        && Objects.equals(this.message, that.message)
        && Objects.equals(this.type, that.type)
        && Objects.equals(this.helpUri, that.helpUri)
        && Objects.equals(this.context, that.context);
  }

  @Override
  public @NotNull String toString() {
    return "ResponseMessageDto{"
        + "code='" + this.code + '\''
        + ", message='" + this.message + '\''
        + ", type=" + this.type
        + ", helpUri='" + this.helpUri + '\''
        + ", context=" + this.context
        + '}';
  }
}
