/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.domain;

import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Specification of unified response structure. It encapsulates usual fields needed for handling
 * different states of the application.
 * <ol>
 *   <li>
 *     <b>requestId</b>: The response is identified by request ID that is important to lookup
 *     all traces of the request/response.
 *   </li>
 *   <li><b>data</b>: Data payload returned by the endpoint.</li>
 *   <li><b>error</b>: Error message with details when there was a problem with processing a
 *   request. </li>
 *   <li><b>messages</b>:
 *   List of additional messages coming together with either correct or error response. Examples of
 *   usages could be:
 *    <ul>
 *       <li><b>with data</b>: Typical usage could be warning message about expiring user's
 *       password; maintenance license; or insecure connection.</li>
 *       <li><b>with error</b>: E.g., for validation error, additional messages may contain
 *       list of all invalid fields. Instead of returning error response for each invalid field,
 *       response may contain all of them.
 *       </li>
 *    </ul>
 *   </li>
 * </ol>
 *
 * @param <D> Type of data structure returned to the client.
 */
public interface Response<D> {

  /**
   * Data payload returned by the endpoint.
   *
   * @return Data payload.
   */
  @Nullable
  D getData();

  /**
   * Error message with details when there was a problem with processing a request.
   *
   * @return Error message.
   */
  @Nullable
  ResponseMessage getError();

  /**
   * List of additional messages coming together with either correct or error response. Examples of
   * usages could be:
   * <ul>
   *    <li><b>with data</b>: Typical usage could be warning message about expiring user's
   *    password; maintenance license; or insecure connection.</li>
   *    <li><b>with error</b>: E.g., for validation error, additional messages may contain
   *    list of all invalid fields. Instead of returning error response for each invalid field,
   *    response may contain all of them.
   *    </li>
   * </ul>
   *
   * @return List of additional messages returned to the user.
   */
  @Nullable
  List<ResponseMessage> getMessages();

  /**
   * Operation specific ID to help lookup all traces of the operation in log files.
   *
   * @return User request unique identifier.
   */
  @NotNull
  String getRequestId();
}
