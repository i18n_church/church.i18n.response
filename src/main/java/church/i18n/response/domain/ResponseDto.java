/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.domain;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


/**
 * Specification of unified  REST response structure. It encapsulates usual fields needed for
 * handling different states of the application.
 * <ol>
 *   <li>
 *     <b>requestId</b>: The response is identified by operation ID that is important to lookup
 *     all traces of the request/response.
 *   </li>
 *   <li><b>data</b>: Data payload returned by the endpoint.</li>
 *   <li><b>error</b>: Error message with details when there was a problem with processing a
 *   request. </li>
 *   <li><b>messages</b>:
 *   List of additional messages coming together with either correct or error response. Examples of
 *   usages could be:
 *    <ul>
 *       <li><b>with data</b>: Typical usage could be warning message about expiring user's
 *       password; maintenance license; or insecure connection.</li>
 *       <li><b>with error</b>: E.g., for validation error, additional messages may contain
 *       list of all invalid fields. Instead of returning error response for each invalid field,
 *       response may contain all of them.
 *       </li>
 *    </ul>
 *   </li>
 * </ol>
 *
 * @param <D> Type of data structure returned to the client.
 */
public class ResponseDto<D> implements Response<D>, Serializable {

  @Serial
  private static final long serialVersionUID = -8629452386411343865L;
  private final @NotNull String requestId;
  private final @Nullable D data;
  //This should be ResponseMessageDto
  private final @Nullable ResponseMessage error;
  private final @Nullable List<ResponseMessage> messages;

  /**
   * Constructor of correct rest response.
   *
   * @param requestId The response is identified by operation ID that is important to lookup all
   *                  traces of the request/response.
   * @param data      Data payload returned by the endpoint.
   * @param messages  List of additional messages coming together with either correct or error
   *                  response. Examples of usages could be:
   *                  <ul>
   *                     <li><b>with data</b>: Typical usage could be warning message about
   *                     expiring user's password; maintenance license; or running out of space.
   *                     </li>
   *                     <li><b>with error</b>: E.g., for validation error, additional messages
   *                     may contain list of all invalid fields. Instead of returning error
   *                     response for each invalid field, response may contain all of them.
   *                     </li>
   *                  </ul>
   */
  public ResponseDto(final @NotNull String requestId, final @Nullable D data,
      final @Nullable List<ResponseMessage> messages) {
    this(requestId, data, null, messages);
  }

  /**
   * Constructor of error rest response.
   *
   * @param requestId The response is identified by operation ID that is important to lookup all
   *                  traces of the request/response.
   * @param error     Error message with details when there was a problem with processing a
   *                  request.
   * @param messages  List of additional messages coming together with either correct or error
   *                  response. Examples of usages could be:
   *                  <ul>
   *                     <li><b>with data</b>: Typical usage could be warning message about
   *                     expiring user's password; maintenance license; or insecure connection.
   *                     </li>
   *                     <li><b>with error</b>: E.g., for validation error, additional messages
   *                     may contain list of all invalid fields. Instead of returning error
   *                     response for each invalid field, response may contain all of them.
   *                     </li>
   *                  </ul>
   */
  public ResponseDto(final @NotNull String requestId, final @Nullable ResponseMessage error,
      final @Nullable List<ResponseMessage> messages) {
    this(requestId, null, error, messages);
  }

  /**
   * Constructor of rest response object with both; data and error. This should be used with caution
   * and not abused. Typical error response does not contain data payload.
   *
   * @param requestId The response is identified by operation ID that is important to lookup all
   *                  traces of the request/response.
   * @param data      Data payload returned by the endpoint.
   * @param error     Error message with details when there was a problem with processing a
   *                  request.
   * @param messages  List of additional messages coming together with either correct or error
   *                  response. Examples of usages could be:
   *                  <ul>
   *                     <li><b>with data</b>: Typical usage could be warning message about
   *                     expiring user's password; maintenance license; or insecure connection.
   *                     </li>
   *                     <li><b>with error</b>: E.g., for validation error, additional messages
   *                     may contain list of all invalid fields. Instead of returning error
   *                     response for each invalid field, response may contain all of them.
   *                     </li>
   *                  </ul>
   */
  public ResponseDto(final @NotNull String requestId, final @Nullable D data,
      final @Nullable ResponseMessage error, final @Nullable List<ResponseMessage> messages) {
    this.requestId = requestId;
    this.data = data;
    this.error = error;
    this.messages = messages;
  }

  @Override
  public @Nullable D getData() {
    return data;
  }

  @Override
  public @Nullable ResponseMessage getError() {
    return error;
  }

  @Override
  public @Nullable List<ResponseMessage> getMessages() {
    return messages;
  }

  @Override
  public @NotNull String getRequestId() {
    return requestId;
  }

  @Override
  public final int hashCode() {
    return Objects.hash(requestId, data, error, messages);
  }

  @Override
  public final boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (!(obj instanceof final ResponseDto<?> that)) {
      return false;
    }
    return Objects.equals(requestId, that.requestId) &&
        Objects.equals(data, that.data) &&
        Objects.equals(error, that.error) &&
        Objects.equals(messages, that.messages);
  }

  @Override
  public String toString() {
    return "ResponseDto{"
        + "requestId='" + requestId + '\''
        + ", data=" + data
        + ", error=" + error
        + ", messages=" + messages
        + '}';
  }
}
