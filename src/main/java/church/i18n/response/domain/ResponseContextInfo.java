/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.domain;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Typical structure of contextual information associated with response message. Holds contextual
 * information associated with response message.The structure of contextual information is
 * following:
 * <ol>
 *  <li><b>name</b>:
 *  Name of the parameter, property or validated field. E.g., email, name.
 *  </li>
 *  <li><b>type</b>: Value type. For example: BOOLEAN, STRING, OBJECT…</li>
 *  <li><b>value</b>:
 *  The original value that was provided by the user, computed value by the system or other value
 *  associated with the message.
 *  </li>
 *  <li><b>format</b>:
 *  Specifies the correct or expected format of the data. When it is not met, you may instruct
 *  consumer of the message by specifying the format of the input parameter your application
 *  expects. E.g., for simple e-mail format it may hold regular expression:
 *  {@code ^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,64}$}.
 *  </li>
 *  <li><b>formatType</b>:
 *  Format argument type. For example when the format of data type is an object you can closely
 *  specify what type of object it is — e.g., JSON, XML. Or similarly, when you specify the
 *  parameter of a string type, you may specify that string is an array or enum format.
 *  </li>
 *  <li><b>message</b>:
 *  Message providing more detailed human-readable information about the parameter.
 *  </li>
 * </ol>
 */
public interface ResponseContextInfo {

  /**
   * Specifies the correct or expected format of data. When it is not met, you may instruct consumer
   * of the message by specifying the format of the input parameter your application expects. E.g.,
   * for simple e-mail format it may hold regular expression: {@code ^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,64}$}.
   *
   * @return Expected format of data.
   */
  @Nullable
  Object getHelp();

  /**
   * Format argument type. For example when the format of data type is an object you can closely
   * specify what type of object it is — e.g., JSON, XML. Or similarly, when you specify the
   * parameter of a string type, you may specify that string is an array or enum format.
   *
   * @return Format type.
   */
  @Nullable
  String getHelpType();

  /**
   * Message providing more detailed human-readable information about the parameter.
   *
   * @return Human-readable description of the parameter.
   */
  @Nullable
  String getMessage();

  /**
   * Name of the parameter, property or validated field. E.g., email, name.
   *
   * @return Name of the parameter.
   */
  @NotNull
  String getName();

  /**
   * Value type. For example: BOOLEAN, STRING, OBJECT…
   *
   * @return Value type.
   */
  @Nullable
  Object getValue();

  /**
   * The original value that was provided by the user, computed value by the system or other value
   * associated with the message.
   *
   * @return Parameter value.
   */

  @Nullable
  String getValueType();
}
