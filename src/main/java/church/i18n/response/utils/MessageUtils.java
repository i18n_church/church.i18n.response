/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.utils;

import church.i18n.processing.message.I18nMessage;
import church.i18n.resources.bundles.PolyglotMultiResourceBundle;
import java.text.MessageFormat;
import java.util.List;
import java.util.Locale;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class MessageUtils {

  private static final Logger log = LoggerFactory.getLogger(MessageUtils.class);

  private MessageUtils() {
    throw new IllegalStateException("Utility class");
  }

  /**
   * Evaluate the message with resource bundle. Provided message code is looked up in files of the
   * resource bundle. Locales are applied in the ordered as they are passed to the method. The first
   * localized message is found, it is used. If you do not provide the code in the message you
   * rather provide only message to be formatted this will still pass. E.g. you may use the method
   * just as {@link MessageFormat}ter for {@link I18nMessage} however this is not it's primary
   * purpose and is not efficient. In the case the code is not found, the code is returned instead.
   * This returned message is then evaluated with parameters provided together with the message.
   *
   * @param bundle  resource bundle to be used
   * @param message message to be translated and formatted
   * @param locales list of locales to use with the bundle. The first localized message that is
   *                found is used.
   * @return localized and formatted {@link I18nMessage}
   */
  public static @NotNull String evaluateMessage(final @NotNull PolyglotMultiResourceBundle bundle,
      final @NotNull I18nMessage message, final @Nullable List<Locale> locales) {
    log.trace("evaluateMessage(bundle = [{}], message = [{}], locales = [{}])", bundle, message,
        locales);
    MessageFormat messageFormat = bundle.getString(message.getCode(), locales)
        .orElse(new MessageFormat(message.getCode(), Locale.ROOT));
    return messageFormat.format(message.getMessageParams());
  }

}
