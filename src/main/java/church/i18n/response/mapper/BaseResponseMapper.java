/*
 * Copyright (c) 2025 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.mapper;

import church.i18n.processing.config.DefaultProcessingExceptionConfig;
import church.i18n.processing.config.ProcessingExceptionConfig;
import church.i18n.processing.logger.LogMapper;
import church.i18n.processing.logger.MessageTypeLogMapper;
import church.i18n.processing.message.ContextInfo;
import church.i18n.processing.message.ContextValue;
import church.i18n.processing.message.DefaultMessageType;
import church.i18n.processing.message.MessageType;
import church.i18n.processing.message.ProcessingMessage;
import church.i18n.processing.security.sanitizer.DefaultSecurityInfoSanitizer;
import church.i18n.processing.security.sanitizer.SecurityInfoSanitizer;
import church.i18n.processing.storage.MessageStorage;
import church.i18n.processing.storage.ProcessingIdProvider;
import church.i18n.processing.storage.ThreadLocalStorage;
import church.i18n.processing.storage.ThreadNameProcessingIdProvider;
import church.i18n.resources.bundles.PolyglotMultiResourceBundle;
import church.i18n.response.domain.ContextInfoDto;
import church.i18n.response.domain.Response;
import church.i18n.response.domain.ResponseContextInfo;
import church.i18n.response.domain.ResponseDto;
import church.i18n.response.domain.ResponseMessage;
import church.i18n.response.domain.ResponseMessageDto;
import church.i18n.response.utils.MessageUtils;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Base data and error mapper into a unified {@link Response} domain object. The default behaviour expects default
 * single thread per request and {@link ThreadNameProcessingIdProvider} is used as default requestId providers. Default
 * message storage is not set. If you need to use it, please set it manually.
 */
public class BaseResponseMapper implements ResponseMapper<Response<?>> {

  private static final Logger log = LoggerFactory.getLogger(BaseResponseMapper.class);
  protected final @Nullable MessageStorage messageStorage;
  private final @NotNull PolyglotMultiResourceBundle bundle;
  private final @NotNull ProcessingIdProvider processingIdProvider;
  private final @NotNull CodeMapper codeMapper;
  private final @NotNull ProcessingExceptionConfig config;
  private final @NotNull SecurityInfoSanitizer exposeSanitizer;
  private final @NotNull LogMapper logMapper;

  private BaseResponseMapper(
      final @NotNull PolyglotMultiResourceBundle bundle,
      final @NotNull ProcessingIdProvider processingIdProvider,
      final @NotNull MessageStorage messageStorage,
      final @NotNull CodeMapper codeMapper,
      final @NotNull ProcessingExceptionConfig config,
      final @NotNull SecurityInfoSanitizer exposeSanitizer,
      final @NotNull LogMapper logMapper) {
    this.bundle = bundle;
    this.processingIdProvider = processingIdProvider;
    this.messageStorage = messageStorage;
    this.codeMapper = codeMapper;
    this.config = config;
    this.exposeSanitizer = exposeSanitizer;
    this.logMapper = logMapper;
  }

  /**
   * Format and translate {@link ContextInfo}.
   *
   * @param bundle      Resource bundle to be used.
   * @param contextInfo Contextual info to be translated and formatted.
   * @param locales     List of locales to use with the bundle. It uses the first localized message it finds.
   * @return Format and translate {@link ContextInfo} that can be presented to the user.
   */
  public @NotNull Optional<ResponseContextInfo> toResponseContextInfo(
      final @NotNull PolyglotMultiResourceBundle bundle, final @NotNull ContextInfo contextInfo,
      final @Nullable List<Locale> locales) {
    log.trace("toResponseContextInfo(bundle = [{}], contextInfo = [{}], locales = [{}])", bundle,
        contextInfo, locales);
    return this.exposeSanitizer.sanitize(contextInfo, this.config).map(ctxInfo ->
        new ContextInfoDto(
            ctxInfo.getName(),
            Optional.ofNullable(ctxInfo.getContext()).map(ContextValue::getValue).orElse(null),
            Optional.ofNullable(ctxInfo.getContext()).map(ContextValue::getValueType).orElse(null),
            Optional.ofNullable(ctxInfo.getHelp()).map(ContextValue::getValue).orElse(null),
            Optional.ofNullable(ctxInfo.getHelp()).map(ContextValue::getValueType).orElse(null),
            Optional.ofNullable(ctxInfo.getMessage())
                .map(message -> MessageUtils.evaluateMessage(bundle, message, locales)).orElse(null)
        ));
  }

  /**
   * Format and translate {@link ProcessingMessage} and return it in {@link ResponseMessage} format.
   *
   * @param message Message to be translated and formatted.
   * @param locales List of locales to use with the bundle. It uses the first localized message it finds.
   * @return Formatted and translated {@link ProcessingMessage} that can be returned to the user.
   */
  public @NotNull Optional<ResponseMessage> toResponseMessage(final @NotNull ProcessingMessage message,
      final @Nullable List<Locale> locales) {
    log.trace("toResponseMessage(message = [{}], locales = [{}])", message, locales);
    Optional<ProcessingMessage> optionalSecMessage = this.exposeSanitizer
        .sanitize(message, this.config);
    return optionalSecMessage.map(secMessage -> {
          String remappedCode = this.codeMapper.remapCode(message.getMessage().getCode());
          String evaluatedMessage = MessageUtils
              .evaluateMessage(this.bundle, message.getMessage(), locales);
          String messageType = Optional.of(message.getMessageType())
              .map(mType -> mType == DefaultMessageType.DEFAULT
                            ? this.config.defaultMessageType()
                            : mType)
              .map(MessageType::getType)
              .orElse(null);
          List<ResponseContextInfo> responseContextInfos = message.getContextInfo()
              .stream()
              .map(contextInfo -> toResponseContextInfo(this.bundle, contextInfo, locales))
              .flatMap(Optional::stream)
              .toList();
          return new ResponseMessageDto(
              remappedCode,
              evaluatedMessage,
              messageType,
              Optional.ofNullable(message.getHelpUri()).map(URI::toString).orElse(null),
              responseContextInfos
          );
        }
    );
  }

  /**
   * Method to map values into a response.
   *
   * @param processingId Value of {@code processingId}
   * @param data         Data to return with the response.
   * @param error        Error message to return.
   * @param locales      Prioritized list of locales used to localize messages. The first localized message that is
   *                     found is used.
   * @param messages     List of additional messages to append with the response.
   * @param <T>          Type of data.
   * @return An instance of {@link Response} object.
   */
  public @NotNull <T> Response<T> toResponse(final @NotNull String processingId, final @Nullable T data,
      final @Nullable ProcessingMessage error, final @Nullable List<Locale> locales,
      final @Nullable ProcessingMessage... messages) {
    log.trace("toResponse(processingId = [{}], data = [{}], error = [{}], locales = [{}], "
        + "messages = [{}])", processingId, data, error, locales, messages);
    List<ResponseMessage> restResponseMessages = Arrays.stream(messages)
        .filter(Objects::nonNull)
        .map(message -> toResponseMessage(message, locales))
        .flatMap(Optional::stream)
        .toList();
    ResponseMessage responseMessage = Optional.ofNullable(error)
        .flatMap(e -> toResponseMessage(e, locales))
        .orElse(null);
    return new ResponseDto<>(
        processingId,
        data,
        responseMessage,
        restResponseMessages
    );
  }

  public static Builder builder() {
    return new Builder();
  }

  @Override
  public @NotNull <T> Response<T> map(final @Nullable T data,
      final @Nullable ProcessingMessage error, final @Nullable List<Locale> locales,
      final @Nullable ProcessingMessage... messages) {
    log.trace("map(data = [{}], error = [{}], locales = [{}], messages = [{}])", data, error,
        locales, messages);
    ProcessingMessage[] allMessages = messages;
    if (this.messageStorage != null) {
      allMessages = Stream.concat(
          this.messageStorage.getAndClear()
              .stream(),
          Arrays.stream(messages)
      ).toArray(ProcessingMessage[]::new);
    }
    return toResponse(processingIdProvider.getProcessingId(), data, error, locales, allMessages);
  }

  /**
   * Get a configuration used by this mapper.
   *
   * @return An instance of configuration class.
   */
  public @NotNull ProcessingExceptionConfig getConfig() {
    log.trace("getConfig()");
    return this.config;
  }

  /**
   * Retrieve a current instance of sanitizer used for exposing data by this mapper. This is a different instance than
   * one used by {@link LogMapper}.
   *
   * @return An instance of security info sanitizer.
   */
  public @NotNull SecurityInfoSanitizer getExposeSanitizer() {
    log.trace("getExposeSanitizer()");
    return this.exposeSanitizer;
  }

  /**
   * Get an instance of a log mapper.
   *
   * @return An instance of {@link LogMapper} used by this mapper.
   */
  public @NotNull LogMapper getLogMapper() {
    log.trace("getLogMapper()");
    return this.logMapper;
  }

  public @Nullable MessageStorage getMessageStorage() {
    return this.messageStorage;
  }

  public static final class Builder {

    private MessageStorage messageStorage;
    private PolyglotMultiResourceBundle bundle = new PolyglotMultiResourceBundle();
    private ProcessingIdProvider processingIdProvider = new ThreadNameProcessingIdProvider();
    private CodeMapper codeMapper = new IdentityCodeMapper();
    private ProcessingExceptionConfig config = DefaultProcessingExceptionConfig.builder().build();
    private SecurityInfoSanitizer exposeSanitizer = new DefaultSecurityInfoSanitizer(
        ProcessingExceptionConfig::defaultSecurityPolicy,
        ProcessingExceptionConfig::exposeSecurityPolicies
    );
    private LogMapper logMapper = new MessageTypeLogMapper(new DefaultSecurityInfoSanitizer(
        ProcessingExceptionConfig::defaultSecurityPolicy,
        ProcessingExceptionConfig::logSecurityPolicies
    ), this.config);

    private Builder() {
    }

    public Builder withBundle(final PolyglotMultiResourceBundle bundle) {
      this.bundle = bundle;
      return this;
    }

    public Builder withProcessingIdProvider(final ProcessingIdProvider processingIdProvider) {
      this.processingIdProvider = processingIdProvider;
      return this;
    }

    public Builder withMessageStorage(final MessageStorage messageStorage) {
      this.messageStorage = messageStorage;
      return this;
    }

    public Builder withCodeMapper(final CodeMapper codeMapper) {
      this.codeMapper = codeMapper;
      return this;
    }

    public Builder withConfig(final ProcessingExceptionConfig config) {
      this.config = config;
      return this;
    }

    public Builder withExposeSanitizer(final SecurityInfoSanitizer exposeSanitizer) {
      this.exposeSanitizer = exposeSanitizer;
      return this;
    }

    public Builder withLogMapper(final LogMapper logMapper) {
      this.logMapper = logMapper;
      return this;
    }

    public BaseResponseMapper build() {
      if (messageStorage == null) {
        messageStorage = new ThreadLocalStorage(processingIdProvider);
      }
      return new BaseResponseMapper(bundle, processingIdProvider, messageStorage, codeMapper,
          config, exposeSanitizer, logMapper);
    }
  }
}
