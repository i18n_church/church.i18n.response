/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.mapper;

import church.i18n.processing.exception.ProcessingException;
import church.i18n.processing.message.ProcessingMessage;
import church.i18n.processing.storage.StorageMessageProvider;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Base mapper of data, error and messages into response.
 *
 * @param <R> Response object type.
 */
public interface ResponseMapper<R> {

  /**
   * Mapper of the exception to response data object. This is the only method that needs to be
   * specified. All other methods are just syntactical sugar for easier manipulation with the
   * response mapping. Method is also responsible for adding messages added to {@link
   * StorageMessageProvider} and possibly clear them from the storage.
   *
   * @param data     Data to be returned to the user.
   * @param error    Primary error message returned to the user.
   * @param locales  Prioritized list of locales to format response messages. When none is
   *                 specified, the {@link Locale#getDefault()} is used.
   * @param messages Messages to include in the response.
   * @param <T>      Type of data.
   * @return Response that could be directly returned by an endpoint.
   */
  @NotNull <T> R map(@Nullable T data, @Nullable ProcessingMessage error,
      @Nullable List<Locale> locales, @Nullable ProcessingMessage... messages);

  /**
   * Mapper of an exception to response data object.
   *
   * @param ex       Exception that represents primary error message returned to the user.
   * @param locales  Prioritized list of locales to format response messages. When none is
   *                 specified, the {@link Locale#getDefault()} is used.
   * @param messages Messages to include in the response.
   * @return Response that might be directly returned by an endpoint.
   */
  default @NotNull R map(final @NotNull ProcessingException ex, final @Nullable List<Locale> locales,
      final @Nullable ProcessingMessage... messages) {
    return map(null, ex, locales, messages);
  }

  /**
   * Mapper of the exception to response data object.
   *
   * @param data     Data to be returned to the user.
   * @param ex       Exception that represents primary error message returned to the user.
   * @param locales  Prioritized list of locales to format response messages. When none is
   *                 specified, the {@link Locale#getDefault()} is used.
   * @param messages Messages to include in the response.
   * @param <T>      Type of data.
   * @return Response that could be directly returned by an endpoint.
   */
  default @NotNull <T> R map(final @Nullable T data, final @Nullable ProcessingException ex,
      final @Nullable List<Locale> locales, final @Nullable ProcessingMessage... messages) {
    return map(data, ex == null ? null : ex.getProcessingMessage(), locales, messages);
  }

  /**
   * Mapper of the exception to response data object.
   *
   * @param error    Primary error message returned to the user.
   * @param locales  Prioritized list of locales to format response messages. When none is
   *                 specified, the {@link Locale#getDefault()} is used.
   * @param messages Messages to include in the response.
   * @return Response that could be directly returned by an endpoint.
   */
  default @NotNull R map(final @NotNull ProcessingMessage error, final @Nullable List<Locale> locales,
      final @Nullable ProcessingMessage... messages) {
    return map(null, error, locales, messages);
  }

  /**
   * Mapper of the exception to response data object.
   *
   * @param ex       Exception that represents primary error message returned to the user.
   * @param locales  Prioritized list of locales to format response messages. When none is
   *                 specified, the {@link Locale#getDefault()} is used.
   * @param messages Messages to include in the response.
   * @return Response that could be directly returned by an endpoint.
   */
  default @NotNull R map(final @NotNull ProcessingException ex, final @Nullable Enumeration<Locale> locales,
      final @Nullable ProcessingMessage... messages) {
    return map(ex, locales == null ? Collections.emptyList() : Collections.list(locales), messages);
  }

  /**
   * Mapper of the exception to response data object.
   *
   * @param data     Data to be returned to the user.
   * @param ex       Exception that represents primary error message returned to the user.
   * @param locales  Prioritized list of locales to format response messages. When none is
   *                 specified, the {@link Locale#getDefault()} is used.
   * @param messages Messages to include in the response.
   * @param <T>      Type of data.
   * @return Response that could be directly returned by an endpoint.
   */
  default @NotNull <T> R map(final @Nullable T data, final @Nullable ProcessingException ex,
      final @Nullable Enumeration<Locale> locales, final @Nullable ProcessingMessage... messages) {
    return map(data, ex, locales == null ? Collections.emptyList() : Collections.list(locales),
        messages);
  }

  /**
   * Mapper of the exception to response data object.
   *
   * @param error    Primary error message returned to the user.
   * @param locales  Prioritized list of locales to format response messages. When none is
   *                 specified, the {@link Locale#getDefault()} is used.
   * @param messages Messages to include in the response.
   * @return Response that could be directly returned by an endpoint.
   */
  default @NotNull R map(final @NotNull ProcessingMessage error,
      final @Nullable Enumeration<Locale> locales, final @Nullable ProcessingMessage... messages) {
    return map(error, locales == null ? Collections.emptyList() : Collections.list(locales),
        messages);
  }

  /**
   * Mapper of the exception to response data object.
   *
   * @param data     Data to be returned to the user.
   * @param error    Primary error message returned to the user.
   * @param locales  Prioritized list of locales to format response messages. When none is
   *                 specified, the {@link Locale#getDefault()} is used.
   * @param messages Messages to include in the response.
   * @param <T>      Type of data.
   * @return Response that could be directly returned by an endpoint.
   */
  default @NotNull <T> R map(final @Nullable T data, final @Nullable ProcessingMessage error,
      final @Nullable Enumeration<Locale> locales, final @Nullable ProcessingMessage... messages) {
    return map(data, error,
        locales == null ? Collections.emptyList() : Collections.list(locales), messages);
  }

}
