module church.i18n.response {
  requires static org.jetbrains.annotations;
  requires transitive org.slf4j;

  requires church.i18n.processing.exception;
  requires church.i18n.resources.bundles;

  exports church.i18n.response.domain;
  exports church.i18n.response.mapper;
  exports church.i18n.response.utils;
}